package app

import (
	"bitbucket.org/Thenor_Baker/bookstore_users-api/controllers/users_controller"
)

func mapUrls() {
	router.POST("/users", users_controller.CreateUser)
	router.GET("/users/:user_id", users_controller.GetUser)
	router.PUT("/users/:user_id", users_controller.UpdateUser)
	router.PATCH("/users/:user_id", users_controller.UpdateUser)
	router.DELETE("/users/:user_id", users_controller.DeleteUser)
	router.GET("/internal/users/search", users_controller.GetUsersByStatus)
}
