package app

import (
	"bitbucket.org/Thenor_Baker/bookstore_users-api/logger"
	"github.com/gin-gonic/gin"
)

var router = gin.Default()

func StartApplication() {
	mapUrls()

	logger.Info("About to start the application...")

	router.Run(":8080")
}
