package users_controller

import (
	"bitbucket.org/Thenor_Baker/bookstore_users-api/domain/users"
	"bitbucket.org/Thenor_Baker/bookstore_users-api/services"
	"bitbucket.org/Thenor_Baker/bookstore_users-api/utils/errors"
	"bitbucket.org/Thenor_Baker/bookstore_users-api/utils/json_utils"
	"github.com/gin-gonic/gin"
	"net/http"
)

const (
	status = "status"
	uID    = "user_id"
)

//binds json parameters and invokes CreateUser service
//returns created user's struct or an error
func CreateUser(c *gin.Context) {
	var user users.User
	/*
		//bytes, err := ioutil.ReadAll(c.Request.Body)
		//if err != nil {
		//	return
		//}
		//if err := json.Unmarshal(bytes, &user); err != nil {
		//	fmt.Println(err.Error())
		//}
	*/
	//the function bellow does the same thing that all the commented code above does
	if err := c.ShouldBindJSON(&user); err != nil {
		restErr := errors.NewBadRequestError("Invalid json body")
		c.JSON(restErr.Status, restErr)
		return
	}
	result, restErr := services.UserService.CreateUser(user)
	if restErr != nil {
		c.JSON(restErr.Status, restErr)
		return
	}

	c.JSON(http.StatusCreated, result.Marshall(c.GetHeader("X-Public") == "true"))
}

func GetUser(c *gin.Context) {
	id, err := json_utils.StringToInt(c.Param(uID))
	if err != nil {
		c.JSON(err.Status, err)
		return
	}
	result, restErr := services.UserService.GetUser(id)
	if restErr != nil {
		c.JSON(restErr.Status, restErr)
		return
	}

	c.JSON(http.StatusOK, result.Marshall(c.GetHeader("X-Public") == "true"))
}

func GetUsersByStatus(c *gin.Context) {
	status := c.Query(status)
	if status == "" {
		c.JSON(http.StatusBadRequest, errors.NewBadRequestError("Invalid json body. Did you fill the 'status' field?"))
		return
	}
	result, restErr := services.UserService.GetUsersByStatus(status)
	if restErr != nil {
		c.JSON(restErr.Status, restErr)
		return
	}

	c.JSON(http.StatusOK, result.Marshall(c.GetHeader("X-Public") == "true"))
}

func UpdateUser(c *gin.Context) {
	id, err := json_utils.StringToInt(c.Param(uID))
	if err != nil {
		c.JSON(err.Status, err)
		return
	}
	var newUser users.User
	if err := c.ShouldBindJSON(&newUser); err != nil {
		restErr := errors.NewBadRequestError("Invalid json body")
		c.JSON(restErr.Status, restErr)
		return
	}
	newUser.Id = id
	isParital := c.Request.Method == http.MethodPatch
	result, restErr := services.UserService.UpdateUser(newUser, isParital)
	if restErr != nil {
		c.JSON(restErr.Status, restErr)
		return
	}
	c.JSON(http.StatusAccepted, result.Marshall(c.GetHeader("X-Public") == "true"))
}

func DeleteUser(c *gin.Context) {
	id, err := json_utils.StringToInt(c.Param(uID))
	if err != nil {
		c.JSON(err.Status, err)
		return
	}
	user := users.User{
		Id: id,
	}
	restErr := services.UserService.DeleteUser(user)
	if restErr != nil {
		c.JSON(restErr.Status, restErr)
		return
	}

	c.JSON(http.StatusAccepted, map[string]string{"status": "deleted"})
}
