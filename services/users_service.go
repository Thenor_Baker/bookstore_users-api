package services

import (
	"bitbucket.org/Thenor_Baker/bookstore_users-api/domain/users"
	"bitbucket.org/Thenor_Baker/bookstore_users-api/utils/errors"
)

var (
	UserService usersServiceInterface = &usersService{}
)

type usersService struct{}

type usersServiceInterface interface {
	CreateUser(u users.User) (*users.User, *errors.RestErr)
	GetUser(id int64) (*users.User, *errors.RestErr)
	UpdateUser(u users.User, isPartial bool) (*users.User, *errors.RestErr)
	DeleteUser(u users.User) *errors.RestErr
	GetUsersByStatus(status string) (users.Users, *errors.RestErr)
}

func (s *usersService) CreateUser(u users.User) (*users.User, *errors.RestErr) {
	//DTO validation (non SQL)
	if restErr := u.Validate(); restErr != nil {
		return nil, restErr
	}
	//DAO validation (SQL)
	if restError := u.Save(); restError != nil {
		return nil, restError
	}
	return &u, nil
}

func (s *usersService) GetUser(id int64) (*users.User, *errors.RestErr) {
	u := users.User{Id: id}
	if restErr := u.Get(); restErr != nil {
		return nil, restErr
	}
	return &u, nil
}

func (s *usersService) UpdateUser(u users.User, isPartial bool) (*users.User, *errors.RestErr) {
	prev, err := UserService.GetUser(u.Id)
	if err != nil {
		return nil, err
	}
	if isPartial {
		if u.FirstName != "" {
			prev.FirstName = u.FirstName
		}
		if u.LastName != "" {
			prev.LastName = u.LastName
		}
		if u.Email != "" {
			prev.Email = u.Email
		}
		if u.Status != "" {
			prev.Status = u.Status
		}
	} else {
		prev.FirstName = u.FirstName
		prev.LastName = u.LastName
		//we can't update user with empty email so we perform a check
		if u.Email == "" {
			u.Email = prev.Email
		} else {
			prev.Email = u.Email
		}
	}
	if err := prev.Update(); err != nil {
		return nil, err
	}
	return prev, nil
}

func (s *usersService) DeleteUser(u users.User) *errors.RestErr {
	return u.Delete()
}

func (s *usersService) GetUsersByStatus(status string) (users.Users, *errors.RestErr) {
	u := users.User{
		Status: status,
	}
	return u.GetByStatus()
}
