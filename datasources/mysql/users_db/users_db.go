package users_db

import (
	"database/sql"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"log"
	"os"
)

const (
	mySQLhost     = "mySQLhost"
	mySQLusername = "mySQLusername"
	mySQLpassword = "mySQLpassword"
	mySQLschema   = "mySQLschema"
)

var (
	Client *sql.DB

	host     = os.Getenv(mySQLhost)
	username = os.Getenv(mySQLusername)
	password = os.Getenv(mySQLpassword)
	schema   = os.Getenv(mySQLschema)
)

func init() {
	dataSource := fmt.Sprintf("%s:%s@tcp(%s)/%s?charset=utf8", username, password, host, schema)
	var err error
	Client, err = sql.Open("mysql", dataSource)
	if err != nil {
		panic(err.Error())
	}
	if err := Client.Ping(); err != nil {
		panic(err.Error())
	}

	log.Println("Database successfully configured!")
}
