package mysql_errors

import (
	"bitbucket.org/Thenor_Baker/bookstore_users-api/utils/errors"
	"fmt"
	"github.com/go-sql-driver/mysql"
	"strings"
)

const (
	errorNoRows = "no rows in result set"
)

func ParseError(err error) *errors.RestErr {
	sqlErr, ok := err.(*mysql.MySQLError)
	if !ok {
		if strings.Contains(err.Error(), errorNoRows) {
			return errors.NewNotFoundError("No matching records with given id")
		}
		fmt.Println(err.Error())
		return errors.NewInternalServerError("Error parsing database response")
	}
	switch sqlErr.Number {
	case 1062:
		return errors.NewBadRequestError(fmt.Sprintf("The data is already exists"))
	}
	return errors.NewInternalServerError("Error processing request")
}
