package json_utils

import (
	"bitbucket.org/Thenor_Baker/bookstore_users-api/utils/errors"
	"strconv"
)

func StringToInt(par string) (int64, *errors.RestErr) {
	id, err := strconv.ParseInt(par, 10, 64)
	if err != nil {
		return 0, errors.NewBadRequestError("User id shoud be a number")
	}
	return id, nil
}
