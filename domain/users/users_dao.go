package users

import (
	"bitbucket.org/Thenor_Baker/bookstore_users-api/datasources/mysql/users_db"
	"bitbucket.org/Thenor_Baker/bookstore_users-api/logger"
	"bitbucket.org/Thenor_Baker/bookstore_users-api/utils/crypto_utils"
	"bitbucket.org/Thenor_Baker/bookstore_users-api/utils/date"
	"bitbucket.org/Thenor_Baker/bookstore_users-api/utils/errors"
	"fmt"
)

const (
	insertUserQuery     = "INSERT INTO users(First_name, Last_name, Email, Date_created, Status, Password) VALUES(?, ?, ?, ?, ?, ?);"
	selectUserByIdQuery = "SELECT ID, First_name, Last_name, Email, Date_created, Status FROM users WHERE ID = ?;"
	updateUserByIdQuery = "UPDATE users SET First_name = ?, Last_name = ?, Email = ?, Status = ? WHERE Id = ?;"
	deleteUserByIdQuery = "DELETE FROM users WHERE Id = ?;"
	getUsersByStatus    = "SELECT Id, First_name, Last_name, Email, Date_created, Status FROM users WHERE Status = ?;"
	dbErr               = "Something went wrong. Database returned an error."
	newUserStatus       = "active"
)

func (u *User) Get() *errors.RestErr {
	stmt, err := users_db.Client.Prepare(selectUserByIdQuery)
	if err != nil {
		logger.Error("Error when trying to prepare `get user` statement", err)
		return errors.NewInternalServerError(dbErr)
	}
	defer stmt.Close()

	//stmt.QueryRow uses only for the one row, so we don't need to close the connection manually
	//but if we need several rows, we use stmt.Query and we DO need defer stmt.close in the case of failure
	result := stmt.QueryRow(u.Id)
	if err := result.Scan(&u.Id, &u.FirstName, &u.LastName, &u.Email, &u.DateCreated, &u.Status); err != nil {
		logger.Error("Error when trying to scan rows after `get user` statement", err)
		return errors.NewInternalServerError(dbErr)
	}
	return nil
}

func (u *User) Save() *errors.RestErr {
	//prepare checks if the is not incorrect answer from the DB before it is executed
	stmt, err := users_db.Client.Prepare(insertUserQuery)
	if err != nil {
		logger.Error("Error when trying to prepare insert statement", err)
		return errors.NewInternalServerError(dbErr)
	}
	defer stmt.Close()

	u.DateCreated = date.GetNowFormatDB()
	u.Password = crypto_utils.GetMd5(u.Password)
	u.Status = newUserStatus
	insertRes, saveErr := stmt.Exec(u.FirstName, u.LastName, u.Email, u.DateCreated, u.Status, u.Password)
	if saveErr != nil {
		logger.Error("Error when trying execute 'insert user' statement", err)
		return errors.NewInternalServerError("Database error")
	}
	uID, err := insertRes.LastInsertId()
	if err != nil {
		logger.Error("Error when trying get last id after 'insert user' query", err)
		return errors.NewInternalServerError(dbErr)
	}
	u.Id = uID
	return nil
}

func (u *User) Update() *errors.RestErr {
	stmt, err := users_db.Client.Prepare(updateUserByIdQuery)
	if err != nil {
		logger.Error("Error when trying to prepare 'update user' statement", err)
		return errors.NewInternalServerError(dbErr)
	}
	defer stmt.Close()

	_, err = stmt.Exec(u.FirstName, u.LastName, u.Email, u.Status, u.Id)
	if err != nil {
		logger.Error("Error when trying execute 'update user' statement", err)
		return errors.NewInternalServerError(dbErr)
	}
	return nil
}

//how to return message like "there is no user with such id"?
func (u *User) Delete() *errors.RestErr {
	stmt, err := users_db.Client.Prepare(deleteUserByIdQuery)
	if err != nil {
		logger.Error("Error when trying prepare 'delete user' statement", err)
		return errors.NewInternalServerError(dbErr)
	}
	defer stmt.Close()

	if _, err = stmt.Exec(u.Id); err != nil {
		logger.Error("Error when trying execute 'delete user' statement", err)
		return errors.NewInternalServerError(dbErr)
	}
	return nil
}

func (u *User) GetByStatus() ([]User, *errors.RestErr) {
	stmt, err := users_db.Client.Prepare(getUsersByStatus)
	if err != nil {
		logger.Error("Error when trying prepare 'get by status' statement", err)
		return nil, errors.NewInternalServerError(dbErr)
	}
	defer stmt.Close()

	rows, err := stmt.Query(u.Status)
	if err != nil {
		logger.Error("Error when trying execute 'get by status' statement", err)
		return nil, errors.NewInternalServerError(dbErr)
	}
	defer rows.Close()

	results := make([]User, 0)
	for rows.Next() {
		var user User
		if err := rows.Scan(&user.Id, &user.FirstName, &user.LastName, &user.Email, &user.DateCreated, &user.Status); err != nil {
			logger.Error("Error when trying scan rows after 'get by status' query", err)
			return nil, errors.NewInternalServerError(dbErr)
		}
		results = append(results, user)
	}

	if len(results) == 0 {
		return nil, errors.NewBadRequestError(fmt.Sprintf("No users matching status: %s", u.Status))
	}
	return results, nil
}
